<?php

namespace Akpranga\Seeder\Facades;

use Illuminate\Support\Facades\Facade;

class Seeder extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Akpranga\Seeder\Seeder::class;
    }
}
