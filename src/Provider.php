<?php

namespace Akpranga\Seeder;

use \Illuminate\Support\ServiceProvider;

/**
 * Provider for package
 */
class Provider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSeeder();
        $this->app->alias('seeder', Seeder::class);
    }

    /**
     * Register the Seeder instance.
     *
     * @return void
     */
    protected function registerSeeder()
    {
        $this->app->singleton(
            'seeder',
            function () {
                return new Seeder();
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return ['seeder', Seeder::class];
    }
}
