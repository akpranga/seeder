<?php

namespace Akpranga\Seeder;

use Illuminate\Support\Collection;

class Seeder
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $seeds;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $this->seeds = Collection::make([]);
    }

    /**
     * Return everything
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(): Collection
    {
        return $this->seeds;
    }

    /**
     * Return only values
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->seeds->values()->toArray();
    }

    /**
     * Get seed by identifier
     *
     * @param string $identifier
     *
     * @return mixed
     */
    public function get(string $identifier)
    {
        return $this->seeds->get($identifier);
    }

    /**
     * @param string $identifier
     * @param        $class
     *
     * @return $this
     */
    public function add(string $identifier, $class)
    {
        $this->seeds->put($identifier, $class);

        return $this;
    }
}
