<?php

namespace Akpranga\Seeder;

use PHPUnit\Framework\TestCase;

/**
 * Class SeederTest
 *
 * @package Akpranga\Seeder
 * @covers  \Akpranga\Seeder\Seeder
 */
class SeederTest extends TestCase
{
    /** @var \Akpranga\Seeder\Seeder */
    private $seeder;

    protected function setUp()
    {
        $this->seeder = new Seeder();
    }

    public function testSeederCollectionIsInitiallyEmpty()
    {
        $this->assertCount(0, $this->seeder->getAll());
    }

    public function testAdd()
    {
        $this->seeder->add('seed', "Tests\Seed");
        $shouldReturn = [
            'Tests\Seed'
        ];
        $this->assertCount(1, $this->seeder->all());
        $this->assertSame("Tests\Seed", $this->seeder->get('seed'));
        $this->assertSame($shouldReturn, $this->seeder->getAll());
    }
}
